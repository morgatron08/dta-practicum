<?php

namespace App;

/*<!--User.php

Created by Morgan Hibbert
2017-11-04

Handles the user database-->*/

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'balance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get User's current balance
     *
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }
}
