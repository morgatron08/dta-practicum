<?php

/*HttpsProtocol.php

 Created by Morgan Hibbert
 2017-11-04

 Redirects the user to HTTPS when requesting HTTP*/


namespace App\Http\Middleware;

use closure;

class HttpsProtocol
{
    public function handle($request, Closure $next)
    {
       // Change the APP_ENV variable in .env to APP_ENV=prod for production release
        // Leave as APP_ENV=local in .env when developing

        if (!$request->secure() && env('APP_ENV') === 'prod')
        {
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}
