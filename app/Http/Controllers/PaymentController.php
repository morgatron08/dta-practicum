<?php

namespace App\Http\Controllers;

/*<!--PaymentController.php

Created by Morgan Hibbert
2017-11-04

Handles the payments between users-->*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Transaction;
use Auth;
use Psr\Log\NullLogger;

define('INC_AMT', 0.25);

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Money added to every users balance from the bank
     *
     * @return void
     */

    public static function accrueVC()
    {
        // Increase every users balance
        DB::table('users')->increment('balance',INC_AMT);


        // Create a new transaction record

        $users = DB::table('users')->get();

        foreach ($users as $user)
        {
            $transaction = new Transaction;

            $transaction->to = $user->email;
            $transaction->from= 'skrilla@bank.com';
            $transaction->amount = INC_AMT;
            $transaction->isRead = FALSE;

            $transaction->save();
        }

        return redirect('home');

    }

    /**
     * Send money from one user to other users
     *
     * @return void
     */

    public function sendMoney()
    {
        // Before sending money, need to make sure the payer has
        // enough money to send and must make sure the payee exists


        $count=0;
        foreach (request('email') as $email) {
            if ($email != Null)
                $count++;
        }

        $usersBalance = Auth::user()->balance;
        $totalAmount = $usersBalance / $count;

        $messages = [
            'max' => "You do not have enough money to complete this transaction. #sadface #getajob " . $count . " " . $totalAmount ,
        ];

        $this->validate(request(), [
            'email' => 'required|array|min:1',
            'email.*' => 'email|nullable',
            'amount' => "numeric|min:0.0|max:$totalAmount"
        ], $messages);

        // Create a new transaction record and transfer the funds.

        foreach (request('email') as $email)
        {
            if ($email != Null)
            {
                $transaction = new Transaction;

                $transaction->to = $email;
                $transaction->from= Auth::user()->email;
                $transaction->amount = request('amount');
                $transaction->isRead = FALSE;

                $transaction->save();



                DB::table('users')
                    ->where('email',$transaction->to)
                    ->increment('balance', $transaction->amount);

                DB::table('users')
                    ->where('email',$transaction->from)
                    ->decrement('balance', $transaction->amount);
            }

        }

        return redirect('home');
    }

}
