<?php

namespace App;

/*<!--Transaction.php

Created by Morgan Hibbert
2017-11-04

Handles the transactions database-->*/

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Transaction extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from', 'to', 'amount',
    ];

    /**
     * Local Transaction ID.
     *
     * @return string
     */
    public function getTransactionIdAttribute()
    {
        return $this->id;
    }

    /**
     * Get Payee
     *
     * @return string
     */
    public function getPayeeAttribute()
    {
        return $this->to;
    }

    /**
     * Get Payer
     *
     * @return string
     */
    public function getPayerAttribute()
    {
        return $this->from;
    }

    /**
     * Total Amount Transferred.
     *
     * @return double
     */
    public function getTransferAmountAttribute()
    {
        return $this->amount;
    }

    public static function markAsRead()
    {
        DB::table('transactions')->where([
            ['to', Auth::user()->email],
            ['isRead',0],])->update(['isRead' => 1]);

        return redirect('home');
    }


}
