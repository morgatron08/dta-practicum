{{--Transaction.php

Created by Morgan Hibbert
2017-11-04

Displays the log of all transactions related to the logged in user--}}

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Transaction Log</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>From</th>
                            <th>To</th>
                            <th>Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>


                    <tbody>

                        @foreach (DB::table('transactions')
                           ->where('from', Auth::user()->email)
                           ->orwhere('to', Auth::user()->email)
                           ->get()
                           as $users)

                        <tr>
                            <td>{{$users->from}}</td>
                            <td>{{$users->to}}</td>
                            <td>{{$users->amount}}</td>
                            <td>{{$users->created_at}}</td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection