{{--home.blade.php

Created by Morgan Hibbert
2017-11-04

The main user dashboard screen shown when the user is logged in--}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type="text/javascript" >
    // The javascript code handles the fading out of the notification number
    $(document).ready(function()
    {


        $("#notificationLink").click(function()
        {
            $("#notificationContainer").fadeToggle(300);
            $("#notification_count").fadeOut("slow");

            // The ajax script handles clearing the notifications once they have been read

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: 'markAsRead'
            });


            //return false;
        });

//Document Click
        $(document).click(function()
        {
            $("#notificationContainer").hide();

        });
//Popup Click
        $("#notificationContainer").click(function()
        {

            return false;
        });

    });
</script>


@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>

            </div>

        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="notificationLink"><i class="fa fa-microphone"></i> Notifications</a>

                        <ul class="dropdown-menu" id="notificationContainer">

                            @foreach (DB::table('transactions')
                                         ->where([
                                         ['to', Auth::user()->email],
                                         ['isRead',0],])
                                         ->get() as $notification)

                                <li>New transfer from {{$notification->from}}</li>
                                <li role="presentation" class="divider"></li>

                            @endforeach

                        </ul>

                    <span class="badge badge-notify" id="notification_count">
                        {{count(DB::table('transactions')
                                         ->where([
                                         ['to', Auth::user()->email],
                                         ['isRead',0],])
                                         ->get())}}

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Account Balance: {{ sprintf("%.2f",Auth::user()->balance) }}</div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="{{ url('/log') }}">View Transaction Log</a></div>

            </div>
        </div>
    </div>
</div>
</div>


<div class="panel-body">
    <form class="form-horizontal" method="POST" action="{{ route('sendMoney') }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="amount" class="col-md-4 control-label">Amount to send to each recipient (VC)</label>

            <div class="col-md-6">
                <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" required autofocus>

            </div>
        </div>

        <div class="form-group">
          <hr>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #1 e-mail (Required)</label>

            <div class="col-md-6">
                <input id="email1" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}" required autofocus>

            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #2 e-mail (Optional)</label>

            <div class="col-md-6">
                <input id="email2" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}">

            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #3 e-mail (Optional)</label>

            <div class="col-md-6">
                <input id="email3" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}">

            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #4 e-mail (Optional)</label>

            <div class="col-md-6">
                <input id="email4" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}">

            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #5 e-mail (Optional)</label>

            <div class="col-md-6">
                <input id="email5" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}">

            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #6 e-mail (Optional)</label>

            <div class="col-md-6">
                <input id="email6" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}">

            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #7 e-mail (Optional)</label>

            <div class="col-md-6">
                <input id="email7" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}">

            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #8 e-mail (Optional)</label>

            <div class="col-md-6">
                <input id="email8" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}">

            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #9 e-mail (Optional)</label>

            <div class="col-md-6">
                <input id="email9" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}">

            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-4 control-label">Recipient #10 e-mail (Optional)</label>

            <div class="col-md-6">
                <input id="email10" type="text" class="form-control" name="email[]" value="{{ old('email[]') }}">

            </div>
        </div>




        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    sendMoney
                </button>
            </div>
        </div>
    </form>
</div>



@if (count($errors))
    <div class="form-group">
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>

        </div>
@endif

{{--Used for internal development--}}

{{--<form class="form-horizontal" method="POST" action="{{ route('accrueMoney') }}">--}}
{{--{{ csrf_field() }}--}}
    {{--<div class="form-group">--}}
        {{--<div class="col-md-6 col-md-offset-4">--}}
            {{--<button type="submit" class="btn btn-primary">--}}
                {{--Accrue Money (For Testing)--}}
            {{--</button>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</form>--}}



@endsection
