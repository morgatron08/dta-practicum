<?php

/*web.php

 Created by Morgan Hibbert
 2017-11-04

 Route handler*/

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/sendMoney', 'PaymentController@sendMoney')->name('sendMoney');

Route::post('/accrueMoney', 'PaymentController@accrueVC')->name('accrueMoney');

Route::get('/log', function () {
    return view('/log');
});

Route::post('/markAsRead', '\App\Transaction@markAsRead')->name('markAsRead');

